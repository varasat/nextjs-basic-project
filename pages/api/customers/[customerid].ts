import type { NextApiRequest, NextApiResponse } from 'next'
import {isValidMethod} from "@/helpers/api";
import fetch from "node-fetch";

export interface Customer {
    id: string
    name: string
}
type Data = {
    customer: Customer
}

type Status = {
    success: boolean
}

export default async function handler(
    req: NextApiRequest,
    res: NextApiResponse<Object|Status>,
): Promise<void> {
    if (!isValidMethod(req.method as string, 'GET')) {
        return res.status(405).send({ success: false })
    }
    const customerId = req.query['customerid']?.toString()
    const backendURL = `${process.env['BACKEND_URL']}`

    if(customerId && backendURL && backendURL == '') {
        return res.status(200).send({customer: {id: customerId, name: "John"}})
    }

    if(customerId && backendURL && backendURL != '') {
        const url = `${backendURL}/customer/get?customerID=${customerId}`
        try {
            const response = await fetch(url);
            if (!response.ok) {
                return res.status(500).send({ success: false })
            }

            const data = await response.json() as Data;
            return res.status(200).send(data);
        } catch (error) {
            return res.status(500).send({ success: false })
        }
    }
}
