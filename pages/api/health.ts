import type { NextApiRequest, NextApiResponse } from 'next'
import {isValidMethod} from "@/helpers/api";


export default async function handler(
    req: NextApiRequest,
    res: NextApiResponse,
): Promise<void> {
    if (!isValidMethod(req.method as string, 'GET')) {
        return res.status(405).send({ success: false })
    }

    return res.status(200).send({ success: true })
    //
    // const { pid } = req.query
    // res.end(`Post: ${pid}`)
    // const customerId = req.query['customerid']?.toString()
    // if(customerId){
    //     return res.status(200).send({
    //             customer:{
    //                 id: customerId,
    //                 name:"jeff",
    //             },
    //         })
    // }
    // return res.status(401).send({ success: false })

    // if (!token || !isToken(token)) {
    //     return res.status(401).send({ success: false })
    // }

    // const url = `${process.env['APPLICATION_API_URL']}/applications/${token}`
    //
    // await proxy(req, res, url, req.method as string, null)
}
