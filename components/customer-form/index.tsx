import {Button, FormControl, FormErrorMessage, FormHelperText, FormLabel, Input} from "@chakra-ui/react";
import {FC, FormEvent, useState} from "react";

export interface CustomerFormProps {
    customerID?: string
}


export const CustomerForm: FC<CustomerFormProps> = ({customerID = ""})=>{
    const [input, setInput] = useState(customerID)
    const [isDirty, setIsDirty] = useState(false)

    const handleInputChange = (e:any) => {
        setInput(e.target.value)
        setIsDirty(true)
    }

    const handleSubmit = (e:FormEvent<HTMLFormElement>) => {
        console.log("FART")
        e.preventDefault()
        // @ts-ignore
        console.log(e.target.elements.customerId.value);
    }

    const isError = input === ''
    return (
        <form onSubmit={(event) => handleSubmit(event) }>
            <FormControl className="text-white dark:text-black" isInvalid={isError && isDirty}>
                <FormLabel className="text-white dark:text-black">Customer ID</FormLabel>
                <Input name="customerId" className="text-white dark:text-black" value={input} onChange={handleInputChange} />
                <FormErrorMessage>Customer ID is required.</FormErrorMessage>
            </FormControl>

            <Button type="submit">Submit</Button>
        </form>

    )
}