"use client"
import React from 'react'
import { useTheme } from "next-themes";
import {Box, Stack, Switch, Text} from "@chakra-ui/react";


const Toggle = () => {
    const { theme, setTheme } = useTheme();
    function handleTheme(){
        theme == "dark"? setTheme('light'): setTheme("dark")
    }
    return (
        <Switch className={"text-white dark:text-gray-800 px-8 py-2 text-2xl md:text-4xl rounded-lg"} size={'lg'} onChange={()=>handleTheme()}></Switch>
    )
}

export default Toggle