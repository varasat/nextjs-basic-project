# nextjs-basic-project

Hello! I'm Andy, and I've been a software engineer since 2015.
I have ADHD and thusly I find it hard to *start* anything.

This repo is done entirely for people like me that need a basic starting point. You may fork it, write your own improvements (I've added some Homework comments in some places that might be helpful).

Don't let that inner saboteur get you. If your interviewer says "Oh yeah the test at home will only be like a 3 hour project", use this project to actually make it 3 hours long.

For any suggestions just ping me a message at [varasatt@gmail.com](varasatt@gmail.com)



This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).


## Getting Started

First, run the development server:

```bash
npm run dev
# or
yarn dev
# or
pnpm dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

You can start editing the page by modifying `pages/index.tsx`. The page auto-updates as you edit the file.

[API routes](https://nextjs.org/docs/api-routes/introduction) can be accessed on [http://localhost:3000/api/health](http://localhost:3000/api/health). This endpoint can be edited in `pages/api/hello.ts`.

The `pages/api` directory is mapped to `/api/*`. Files in this directory are treated as [API routes](https://nextjs.org/docs/api-routes/introduction) instead of React pages.

This project uses [`next/font`](https://nextjs.org/docs/basic-features/font-optimization) to automatically optimize and load Inter, a custom Google Font.

As an example of interaction of an API with an external API please use [this repo](https://gitlab.com/varasat/backend-api-go-base) and the `http://localhost:8080/customer/get?customerID={your customer id here here}` endpoint

## Features & To-dos
 
- [X] Add next.js
- [X] Add Tailwind 
- [X] Add Chakra UI
- [X] Create a couple of components
- [X] Add Dark Mode
- [X] Add an Api route either to a local server or mocked
- [ ] Add Jest 
- [ ] Add Coverage
 
## Status
Active 